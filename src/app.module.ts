import { Module } from '@nestjs/common';
import { AppController } from './app.controller';
import { AppService } from './app.service';
import { ProjectModule } from './modules/project/project.module';
import { TypeModule } from './modules/type/type.module';
import { DatabaseModule } from './modules/database/database.module';

@Module({
  imports: [ProjectModule, TypeModule],
  controllers: [AppController],
  providers: [AppService],
})
export class AppModule {}
