import { Sequelize } from 'sequelize-typescript';
import { Type } from '../type/type.model';
import { Project } from '../project/project.model';
export const databaseProviders = [
  {
    provide: 'SEQUELIZE',
    useFactory: async () => {
      const sequelize = new Sequelize({
        dialect: 'postgres',
        host: 'localhost',
        port: 5432,
        username: 'postgres',
        password: '123456',
        database: 'project_management',
      });
      sequelize.addModels([Type, Project]);
      await sequelize.sync();
      return sequelize;
    },
  },
];
