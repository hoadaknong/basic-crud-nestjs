import { Controller, Delete, Get, Post, Put } from '@nestjs/common';
import { ProjectService } from './project.service';
import { ResponseData } from 'src/global/models/ResponseData.model';
import { HttpStatus } from 'src/global/enum/HttpStatus.enum';
import { MessageResponse } from 'src/global/enum/MessageResponse.enum';

@Controller('project')
export class ProjectController {
  constructor(private readonly projectService: ProjectService) {}
  @Get()
  getProject(): ResponseData<string> {
    try {
      return new ResponseData<string>(
        this.projectService.getProject(),
        HttpStatus.OK,
        MessageResponse.SUCCESS,
      );
    } catch {
      return new ResponseData<string>(
        null,
        HttpStatus.BAD_REQUEST,
        MessageResponse.ERORR,
      );
    }
  }

  @Post()
  createProject(): ResponseData<string> {
    try {
      return new ResponseData<string>(
        this.projectService.createProject(),
        HttpStatus.OK,
        MessageResponse.SUCCESS,
      );
    } catch {
      return new ResponseData<string>(
        null,
        HttpStatus.BAD_REQUEST,
        MessageResponse.ERORR,
      );
    }
  }

  @Get(':id')
  detailProject(): ResponseData<string> {
    try {
      return new ResponseData<string>(
        this.projectService.getProject(),
        HttpStatus.OK,
        MessageResponse.SUCCESS,
      );
    } catch {
      return new ResponseData<string>(
        null,
        HttpStatus.BAD_REQUEST,
        MessageResponse.ERORR,
      );
    }
  }

  @Put(':id')
  updateProject(): ResponseData<string> {
    try {
      return new ResponseData<string>(
        this.projectService.updateProject(),
        HttpStatus.OK,
        MessageResponse.SUCCESS,
      );
    } catch {
      return new ResponseData<string>(
        null,
        HttpStatus.BAD_REQUEST,
        MessageResponse.ERORR,
      );
    }
  }

  @Delete(':id')
  deleteProject(): ResponseData<string> {
    try {
      return new ResponseData<string>(
        this.projectService.deleteProject(),
        HttpStatus.OK,
        MessageResponse.SUCCESS,
      );
    } catch {
      return new ResponseData<string>(
        null,
        HttpStatus.BAD_REQUEST,
        MessageResponse.ERORR,
      );
    }
  }
}
