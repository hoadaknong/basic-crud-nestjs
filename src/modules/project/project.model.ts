import {
  Model,
  Table,
  Column,
  CreatedAt,
  UpdatedAt,
  BelongsTo,
  ForeignKey,
} from 'sequelize-typescript';
import { Type } from '../type/type.model';

@Table
export class Project extends Model {
  @Column({ unique: true })
  code: number;

  @Column({ unique: true })
  name: string;

  @Column
  status: boolean;

  @ForeignKey(() => Type)
  @Column({ field: 'type_id' })
  typeId: number;

  @BelongsTo(() => Type)
  type: Type;

  @CreatedAt
  createdAt: Date;

  @UpdatedAt
  updatedAt: Date;
}
