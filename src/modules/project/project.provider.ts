import { Project } from './project.model';

export const projectProviders = [
  {
    provide: 'PROJECT_REPOSITORY',
    useValue: Project,
  },
];
