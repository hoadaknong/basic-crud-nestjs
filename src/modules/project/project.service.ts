import { Inject, Injectable } from '@nestjs/common';
import { Project } from './project.model';

@Injectable()
export class ProjectService {
  constructor(
    @Inject('PROJECT_REPOSITORY')
    private readonly projectRepository: typeof Project,
  ) {}

  getProjects(): string {
    return 'LIST PROJECT';
  }

  createProject(): string {
    return 'POST PROJECT';
  }

  getProject(): string {
    return 'DETAIL PROJECT';
  }

  updateProject(): string {
    return 'UPDATE PROJECT';
  }

  deleteProject(): string {
    return 'DELETE PROJECT';
  }
}
