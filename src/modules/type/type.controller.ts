import {
  BadRequestException,
  Body,
  Controller,
  Delete,
  Get,
  Logger,
  Param,
  Post,
  Put,
  Query,
} from '@nestjs/common';
import { ResponseData } from 'src/global/models/ResponseData.model';
import { HttpStatus } from 'src/global/enum/HttpStatus.enum';
import { MessageResponse } from 'src/global/enum/MessageResponse.enum';
import { TypeService } from './type.service';
import { Type } from './type.model';
import { TypeDto } from './type.dto';

@Controller('type')
export class TypeController {
  private readonly logger = new Logger(TypeController.name);
  constructor(private readonly typeService: TypeService) {}
  @Get()
  async getTypes(): Promise<ResponseData<Type[]>> {
    this.logger.log('Fetching type list...');
    return new ResponseData<Type[]>(
      await this.typeService.getTypes(),
      HttpStatus.OK,
      MessageResponse.SUCCESS,
    );
  }

  @Post()
  async createType(@Body() typeData: TypeDto): Promise<ResponseData<Type>> {
    this.logger.log('Create new type...');
    try {
      return new ResponseData<Type>(
        await this.typeService.createType(typeData),
        HttpStatus.OK,
        MessageResponse.SUCCESS,
      );
    } catch (error: any) {
      this.logger.error(error.message);
      throw new BadRequestException(error.message);
    }
  }

  @Get(':id')
  async getType(@Param('id') id: number): Promise<ResponseData<Type>> {
    try {
      return new ResponseData<Type>(
        await this.typeService.getType(id),
        HttpStatus.OK,
        MessageResponse.SUCCESS,
      );
    } catch (error: any) {
      return new ResponseData<any>(
        null,
        HttpStatus.BAD_REQUEST,
        MessageResponse.ERORR,
      );
    }
  }

  @Put(':id')
  async updateType(
    @Param('id') id: number,
    @Body() dto: TypeDto,
  ): Promise<ResponseData<[number, Type[]]>> {
    try {
      return new ResponseData<[number, Type[]]>(
        await this.typeService.updateType(id, dto),
        HttpStatus.OK,
        MessageResponse.SUCCESS,
      );
    } catch {
      return new ResponseData<any>(
        null,
        HttpStatus.BAD_REQUEST,
        MessageResponse.ERORR,
      );
    }
  }

  @Delete(':id')
  async deleteType(@Param('id') id): Promise<ResponseData<void>> {
    try {
      return new ResponseData<void>(
        await this.typeService.remove(id),
        HttpStatus.OK,
        MessageResponse.SUCCESS,
      );
    } catch {
      return new ResponseData<any>(
        null,
        HttpStatus.BAD_REQUEST,
        MessageResponse.ERORR,
      );
    }
  }
}
