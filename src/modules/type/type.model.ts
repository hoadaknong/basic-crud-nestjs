import {
  Model,
  Table,
  Column,
  CreatedAt,
  UpdatedAt,
  HasMany,
} from 'sequelize-typescript';
import { Project } from '../project/project.model';

@Table
export class Type extends Model {
  @Column({ unique: true })
  private name: string;

  @CreatedAt
  createdAt: Date;

  @UpdatedAt
  updatedAt: Date;

  @HasMany(() => Project)
  private projects: Project[];
}
