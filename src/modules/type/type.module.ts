import { Module } from '@nestjs/common';
import { TypeService } from './type.service';
import { TypeController } from './type.controller';
import { DatabaseModule } from '../database/database.module';
import { typeProviders } from './type.provider';

@Module({
  imports: [DatabaseModule],
  controllers: [TypeController],
  providers: [TypeService, ...typeProviders],
})
export class TypeModule {}
