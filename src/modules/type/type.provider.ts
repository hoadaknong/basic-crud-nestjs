import { Type } from './type.model';

export const typeProviders = [
  {
    provide: 'TYPE_REPOSITORY',
    useValue: Type,
  },
];
