import { BadRequestException, Inject, Injectable } from '@nestjs/common';
import { Type } from './type.model';
import { TypeDto } from './type.dto';
import { type } from 'os';

@Injectable()
export class TypeService {
  constructor(
    @Inject('TYPE_REPOSITORY')
    private typeRepository: typeof Type,
  ) {}
  async getTypes(): Promise<Type[]> {
    return this.typeRepository.findAll();
  }

  async createType(typeData: TypeDto): Promise<Type> {
    return this.typeRepository.create({ name: typeData.name });
  }

  async getType(id: number): Promise<Type> {
    return this.typeRepository.findByPk(id);
  }

  async updateType(id: number, typeData: TypeDto): Promise<[number, Type[]]> {
    return this.typeRepository.update(
      { name: typeData.name },
      {
        where: { id },
        returning: true,
      },
    );
  }

  async remove(id: number): Promise<void> {
    await this.typeRepository.destroy({ where: { id } });
  }
}
